# Word_Counter

#### 介绍
统计纯英文文本中的字符数，单词数，句子数等。

#### 软件架构
采用C++编写，使用命令行运行.exe文件。代码包括读取命令行内容函数，读取文本内容函数，统计函数以及输出函数。


#### 安装教程

1.  编译环境是Dev-C++
2.  下载WordCounter文件即可
3.  WordCounter.exe即为运行程序

#### 使用说明

1.  将需要统计的文本（纯英文，包括标点符号等）放入WordCounter文件夹中。
2.  使用Windows的命令行窗口运行WordCounter.exe。
3.  输入参数 -* -***.txt<br />
    例如 ：-c -file.txt  //统计字符数<br />
           -w -file.txt  //统计单词数<br />
           -l -file.txt  //统计行数<br />
           -s -file.txt  //统计句子数<br />
           -e -file.txt  //统计空行数<br />
           -d -file.txt  //统计注释行数<br />

#### 文件列表及其相关说明<br />
1.README.md文件代码的相关说明<br />
2.WordCounter_v0.1.cpp 空项目，代码框架<br />
3.WordCounter_v0.2.cpp 第一代版本，可以实现字符、单词、句子的统计<br />
4.WordCounter_v0.3.cpp 第二代版本，在第一代的基础上加入了行数、空行、注释行的统计<br />
5.WordCounter.exe 代码的运行程序<br />
6.***.txt 需要统计的纯英文文本<br />

#### 例程运行及其相关结果
file.txt内容
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/1014/220148_ced90450_9706506.png "屏幕截图.png")
调出命令行窗口，指定WordCounter.exe路径，运行WordCounter.exe
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/1014/222211_1c60c36c_9706506.png "屏幕截图.png")
1. 运行 -c file.txt
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/1014/221051_91168d84_9706506.png "屏幕截图.png")  
2. 运行 -w file.txt
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/1014/221320_2421270e_9706506.png "屏幕截图.png")
3. 运行 -l file.txt
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/1014/221527_9635f934_9706506.png "屏幕截图.png")
4. 运行 -s file.txt
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/1014/221555_0c37e8ee_9706506.png "屏幕截图.png")
5. 运行 -e file.txt
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/1014/221948_ed0f9bb5_9706506.png "屏幕截图.png")
6. 运行 -d file.txt
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/1014/222011_161420f5_9706506.png "屏幕截图.png")