#include<fstream>
#include<iostream>
#include<string>
using namespace std;
class WC{
private:
	long charn;               //字符数
	int linen;                //行数
	int wordn;                //单词数
	int Sentn;                //句子数
 	int Elinen;               //空行数
	int Dlinen;               //注释行数	 	
	int ordern;				  //命令行数 
	string text;          	  //文本内容 
	char order[100];          //命令行内容 
	char filename[10];        //文件名 
public:
	void DuQuOrder(); 		  
	void FileOpen();  
	void WordCount();                                  
	void Output(); 
};
//功能：读取命令行内容,保存文件名（***.txt）和参数（-*） 
void WC::DuQuOrder(){
	ordern = 0;
	int a,b = 0;
	cin.get(order,1000);
	for(int i = 0;i<100;i++){
		if(int(order[i]) == 0){
			ordern = i;
			break;
		}
		if(int(order[i]) == 45) 
			a = i + 3;
		if(int(order[i]) == 46 && order[i+1] == 116)
			b = i + 3;
	}
	for(int i = 0;i <= b-a+1;i++){
		filename[i] = order[i+a];
	}	
}
//功能：打开文本文件，保存文本内容 
void WC::FileOpen(){
	linen = 0;
	Elinen = 0;
	ifstream myfile(filename);
	if (!myfile)
	{
		cerr << "Open error! Please check the filename." << endl;
	} 
	myfile.seekg(0);
	string line;
    while(getline(myfile,line)){
    	text = text + line; 
    	linen++;			//统计行数 
    	if (line.length() == 0) 
			Elinen++;		//统计空行数 
	}
    for(int i = 0;i < 1000;i++){
    	int ascii = text[i] ;
    	if(ascii == 0){
    		charn = i;		//记录字符数 
    		break;
    	}
	}
    myfile.close() ;
}
//功能：统计文本中单词、句子等的数量 
void WC::WordCount(){
	wordn = 0;
	Sentn = 0;
	Dlinen = 0;
	for(int i = 0;i < charn;i++){
		int ascii = text[i] ;
		int asciiN = text[i+1] ;
		if( (ascii == 32 || ascii == 46 || ascii == 49 || ascii == 63) && asciiN != 32 ){
			wordn++;		//统计单词数 
		}
		if(ascii == 46 || ascii == 33 || ascii == 63){
			Sentn++;		//统计句子数 
		}
		if ((ascii == 47 ) && (asciiN == 47 || asciiN == 42)){
			Dlinen++;		//统计注释行 
		}
	}
}
//功能：根据命令行内容打印相应的字符、单词数
// -c 输出字符数 
// -w 输出单词数 
// -l 输出行数    
// -s 输出  
// -e 输出空行数  
// -d 输出注释行数 
void WC::Output()
{
	for (int i = 0; i < ordern; i++)
	{
		int oascii = order[i];
		int oasciiN = order[i+1];
		if (oascii == 45 && oasciiN == 99)						 
			cout << "字符数 : " << charn << endl;
		else if (oascii == 45 && oasciiN == 119)				
			cout << "单词数 : " << wordn << endl;
		else if (oascii == 45 && oasciiN == 108)			
			cout << "行数 : " << linen << endl;
		else if (oascii == 45 && oasciiN == 115)				
			cout << "句子数 : " << Sentn << endl;
		else if (oascii == 45 && oasciiN == 101)			
			cout << "空行数 : " << Elinen << endl;
		else if (oascii == 45 && oasciiN == 100)					
			cout << "注释行数 : " << Dlinen << endl;
	}
}

int main() {
	WC file;
	file.DuQuOrder();
	file.FileOpen();
	file.WordCount();
	file.Output();
	return 0;
}
