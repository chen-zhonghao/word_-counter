#include<fstream>
#include<iostream>
#include<string>
using namespace std;
class WC{
private:
	long charn;               //字符数
	int wordn;                //单词数
	int Sentn;                //句子数	 	
	int ordern;				  //命令行数 
	string text;          	  //文本内容 
	char order[100];          //命令行内容 
	char filename[10];        //文件名 
public:
	void DuQuOrder(); 		  
	void FileOpen();  
	void WordCount();                                  
	void Output(); 
};
 
void WC::DuQuOrder(){
	ordern = 0;
	int a,b = 0;
	cin.get(order,1000);
	for(int i = 0;i<100;i++){
		if(int(order[i]) == 0){
			ordern = i;
			break;
		}
		if(int(order[i]) == 45) 
			a = i + 3;
		if(int(order[i]) == 46 && order[i+1] == 116)
			b = i + 3;
	}
	for(int i = 0;i <= b-a+1;i++){
		filename[i] = order[i+a];
	}	
}

void WC::FileOpen(){
	ifstream myfile(filename);
	if (!myfile)
	{
		cerr << "Open error! Please check the filename." << endl;
	} 
	myfile.seekg(0);
	string line;
    while(getline(myfile,line)){
    	text = text + line; 
	}
    for(int i = 0;i < 1000;i++){
    	int ascii = text[i] ;
    	if(ascii == 0){
    		charn = i;		//记录字符数 
    		break;
    	}
	}
    myfile.close() ;
}

void WC::WordCount(){
	wordn = 0;
	Sentn = 0;
	Dlinen = 0;
	for(int i = 0;i < charn;i++){
		int ascii = text[i] ;
		int asciiN = text[i+1] ;
		if( (ascii == 32 || ascii == 46 || ascii == 49 || ascii == 63) && asciiN != 32 ){
			wordn++;		//统计单词数 
		}
		if(ascii == 46 || ascii == 33 || ascii == 63){
			Sentn++;		//统计句子数 
		}
	}
}

void WC::Output()
{
	for (int i = 0; i < ordern; i++)
	{
		int oascii = order[i];
		int oasciiN = order[i+1];
		if (oascii == 45 && oasciiN == 99)						 
			cout << "字符数 : " << charn << endl;
		else if (oascii == 45 && oasciiN == 119)				
			cout << "单词数 : " << wordn << endl;
		else if (oascii == 45 && oasciiN == 115)				
			cout << "句子数 : " << Sentn << endl;
	}
}

int main() {
	WC file;
	file.DuQuOrder();
	file.FileOpen();
	file.WordCount();
	file.Output();
	return 0;
}
